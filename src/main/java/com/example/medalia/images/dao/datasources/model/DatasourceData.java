package com.example.medalia.images.dao.datasources.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DatasourceData {
    private Long id;
    private String host;
    private String schema;
    private String user;
    private String password;
    private String driver;
}
