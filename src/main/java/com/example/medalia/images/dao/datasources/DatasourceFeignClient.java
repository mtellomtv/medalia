package com.example.medalia.images.dao.datasources;

import com.example.medalia.images.dao.datasources.model.DatasourceData;
import com.example.medalia.images.dao.datasources.model.DomainData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "datasources", url = "http://localhost:8080")
public interface DatasourceFeignClient {

    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, path = "/getDatabases")
    List<DatasourceData> getDatasources();

    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, path = "/getDBForDomain/{domainName}")
    DomainData getDomain(@PathVariable String domainName);
}
