package com.example.medalia.images.dao.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface ImageRepository extends PagingAndSortingRepository<ImageEntity, Long> {
}
