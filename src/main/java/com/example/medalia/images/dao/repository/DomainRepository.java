package com.example.medalia.images.dao.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DomainRepository extends PagingAndSortingRepository<DomainEntity, Long> {

    @Query("select d from DomainEntity d where d.name=:name")
    DomainEntity findByName(String name);
}
