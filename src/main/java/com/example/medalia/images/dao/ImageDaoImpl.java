package com.example.medalia.images.dao;

import com.example.medalia.images.config.ClientDatabaseContextHolder;
import com.example.medalia.images.config.DynamicDataSource;
import com.example.medalia.images.dao.datasources.DatasourceFeignClient;
import com.example.medalia.images.dao.datasources.model.DatasourceData;
import com.example.medalia.images.dao.datasources.model.DomainData;
import com.example.medalia.images.dao.repository.DomainEntity;
import com.example.medalia.images.dao.repository.ImageEntity;
import com.example.medalia.images.dao.repository.DomainRepository;
import com.example.medalia.images.dao.repository.ImageRepository;
import com.example.medalia.images.model.Domain;
import com.example.medalia.images.model.Image;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ImageDaoImpl implements ImageDao {

    private final DomainRepository domainRepository;
    private final ImageRepository imageRepository;
    private final DataSource dataSource;
    private final DatasourceFeignClient datasourceFeignClient;

    public ImageDaoImpl(DomainRepository domainRepository,
                        ImageRepository imageRepository, @Qualifier("dynamicDatasource") DataSource dynamicDatasource,
                        DatasourceFeignClient datasourceFeignClient) {
        this.domainRepository = domainRepository;
        this.imageRepository = imageRepository;
        this.dataSource = dynamicDatasource;
        this.datasourceFeignClient = datasourceFeignClient;
    }

    @Override
    public List<Domain> getAllImages(Integer page, Integer size){
        changeDataSource();

        Pageable pageable = PageRequest.of(page, size);
        Page<DomainEntity> domains = domainRepository.findAll(pageable);
        return domains.getContent()
                .stream()
                .map(domainEntity -> mapDomainEntityToDomain(domainEntity))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    @Retry(name = "setAllImagesRetry")
    public List<Domain> setAllImages(List<Domain> domains) {


        for (Domain domain : domains) {
            ClientDatabaseContextHolder.set(domain.getName());
            changeDataSource();

            DomainEntity domainEntity = domainRepository.findByName(domain.getName());

            List<ImageEntity> imageEntities = domain.getImages().stream()
                    .map(image -> mapImageToImageEntity(image, domainEntity))
                    .collect(Collectors.toList());

            imageRepository.saveAll(imageEntities);
        }

        return domains;
    }

    private void changeDataSource() {

        DriverManagerDataSource changedDataSource = getDataSource();

        ((DynamicDataSource)dataSource).setDefaultTargetDataSource(changedDataSource);
    }

    private DriverManagerDataSource getDataSource() {
        String datasourceName = ClientDatabaseContextHolder.getClientDatabase();

        DomainData domainData = datasourceFeignClient.getDomain(datasourceName);
        List<DatasourceData> listDataSourceData = datasourceFeignClient.getDatasources();

        DatasourceData currentDataSource = listDataSourceData.stream()
                .filter(datasourceData -> datasourceData.getId().equals(domainData.getDbId()))
                .findFirst()
                .orElse(null);

        return createDataSource(currentDataSource);
    }

    private DriverManagerDataSource createDataSource(DatasourceData currentDataSource) {
        DriverManagerDataSource changedDataSource
                = new DriverManagerDataSource();
        changedDataSource.setDriverClassName(currentDataSource.getDriver());
        changedDataSource.setUrl(currentDataSource.getHost());
        changedDataSource.setUsername(currentDataSource.getUser());
        changedDataSource.setPassword(currentDataSource.getPassword());
        return changedDataSource;
    }

    private Domain mapDomainEntityToDomain(DomainEntity domainEntity) {
        return Domain.builder()
                .id(domainEntity.getId())
                .name(domainEntity.getName())
                .images(domainEntity.getImages()
                        .stream()
                        .map(imageEntity -> mapImageEntityToImage(imageEntity))
                        .collect(Collectors.toList()))
                .build();
    }

    private Image mapImageEntityToImage(ImageEntity imageEntity) {
        return Image.builder()
                .id(imageEntity.getId())
                .path(imageEntity.getPath())
                .build();
    }

    private DomainEntity mapDomainToDomainEntity(Domain domain) {
        DomainEntity domainEntity = new DomainEntity();
        domainEntity.setId(domain.getId());
        domainEntity.setName(domain.getName());
        domainEntity.setImages(domain.getImages()
                .stream()
                .map(image -> mapImageToImageEntity(image, domainEntity))
                .collect(Collectors.toList()));
        return domainEntity;
    }

    private ImageEntity mapImageToImageEntity(Image image, DomainEntity domainEntity) {
        ImageEntity imageEntity = new ImageEntity();
        imageEntity.setId(image.getId());
        imageEntity.setPath(image.getPath());
        imageEntity.setDomain(domainEntity);
        return imageEntity;
    }

}
