package com.example.medalia.images.web.dto;

import com.example.medalia.images.model.Image;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Builder
public class DomainDto {
    private String domain;
    private List<String> images;
}