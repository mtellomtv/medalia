package com.example.medalia.images.web;

import com.example.medalia.images.web.dto.DomainDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

public interface ImageController {

    //We can wrote swagger here and to have the controller cleaner
    List<DomainDto> getAllImages(Integer page, Integer size, String domainId);

    List<DomainDto> setAllImages(List<DomainDto> domainDto);
}
