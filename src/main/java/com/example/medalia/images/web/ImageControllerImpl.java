package com.example.medalia.images.web;

import com.example.medalia.images.config.ClientDatabaseContextHolder;
import com.example.medalia.images.model.Domain;
import com.example.medalia.images.model.Image;
import com.example.medalia.images.service.ImageService;
import com.example.medalia.images.web.dto.DomainDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/images")
public class ImageControllerImpl implements ImageController {

    private final ImageService imageService;

    public ImageControllerImpl(ImageService imageService) {
        this.imageService = imageService;
    }

    @Override
    @ResponseStatus(OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DomainDto> getAllImages(@RequestParam(defaultValue = "0", required = false) Integer page,
                                        @RequestParam(defaultValue = "50", required = false) Integer size,
                                        @RequestParam String domainId){

        ClientDatabaseContextHolder.set(domainId);
        List<Domain> allImages = imageService.getAllImages(page, size);
        ClientDatabaseContextHolder.clear();

        return allImages.stream()
                .map(domain -> mapDomainToDomainDto(domain))
                .collect(Collectors.toList());

    }

    @Override
    @ResponseStatus(OK)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<DomainDto> setAllImages(@RequestBody List<DomainDto> domainsDto){

        List<Domain> domains = domainsDto.stream()
                .map(domainDto -> mapDomainDtoToDomain(domainDto))
                .collect(Collectors.toList());
        List<Domain> allImages = imageService.setAllImages(domains);
        ClientDatabaseContextHolder.clear();

        return allImages.stream()
                .map(domain -> mapDomainToDomainDto(domain))
                .collect(Collectors.toList());

    }

    private DomainDto mapDomainToDomainDto(Domain domain) {
        return DomainDto.builder()
                .domain(domain.getName())
                .images(domain.getImages()
                        .stream()
                        .map(image -> image.getPath())
                        .collect(Collectors.toList()))
                .build();
    }

    private Domain mapDomainDtoToDomain(DomainDto domainDto) {
        return Domain.builder()
                .name(domainDto.getDomain())
                .images(domainDto.getImages()
                        .stream()
                        .map(image -> Image.builder()
                                .path(image).build())
                        .collect(Collectors.toList()))
                .build();
    }
}
