package com.example.medalia.images.service;

import com.example.medalia.images.dao.ImageDao;
import com.example.medalia.images.model.Domain;
import org.springframework.stereotype.Service;

import java.util.List;

// This class can be removed in simple CRUD
@Service
public class ImageServiceImpl implements ImageService {

    private final ImageDao imageDao;

    public ImageServiceImpl(ImageDao imageDao) {
        this.imageDao = imageDao;
    }

    @Override
    public List<Domain> getAllImages(Integer page, Integer size){
        return imageDao.getAllImages(page, size);
    }

    @Override
    public List<Domain> setAllImages(List<Domain> domain) {
        return imageDao.setAllImages(domain);
    }
}
