package com.example.medalia.images.service;

import com.example.medalia.images.model.Domain;
import com.example.medalia.images.web.dto.DomainDto;

import java.util.List;

public interface ImageService {
    List<Domain> getAllImages(Integer page, Integer size);

    List<Domain> setAllImages(List<Domain> domain);
}
