package com.example.medalia;

import com.example.medalia.images.dao.repository.DomainEntity;
import com.example.medalia.images.dao.repository.DomainRepository;
import com.example.medalia.images.dao.repository.ImageEntity;
import com.example.medalia.images.dao.repository.ImageRepository;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.spec.internal.HttpStatus;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static com.example.medalia.testutil.SourceReader.getResourceAsString;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureWireMock
public class ImagesDataIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ImageRepository imageRepository;

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:images/integration/getallimages/db/init_images.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:common/db/clean.sql")
    })
    void getAllImages() throws Exception {

        stubFor(WireMock.get(urlEqualTo("/getDatabases"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withBody(getResourceAsString("images/integration/getallimages/json/alldatasources.json"))));

        stubFor(WireMock.get(urlEqualTo("/getDBForDomain/www.prueba.com"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withBody(getResourceAsString("images/integration/getallimages/json/domain.json"))));

        mockMvc.perform(get("/images?domainId=www.prueba.com")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(getResourceAsString("images/integration/getallimages/json/resultallimages.json")));
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:images/integration/setallimages/db/init_images.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:common/db/clean.sql")
    })
    void setAllImages() throws Exception {

        List<String> imagesToSave = List.of("image1.jpg","somepath/anotherpath/image2.gif",
                "image3.jpg","somepath/anotherpath/image4.gif");

        stubFor(WireMock.get(urlEqualTo("/getDatabases"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withBody(getResourceAsString("images/integration/setallimages/json/alldatasources.json"))));

        stubFor(WireMock.get(urlEqualTo("/getDBForDomain/www.domain1.com"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withBody(getResourceAsString("images/integration/setallimages/json/domain.json"))));

        stubFor(WireMock.get(urlEqualTo("/getDBForDomain/www.domain2.com"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withBody(getResourceAsString("images/integration/setallimages/json/domain.json"))));

        mockMvc.perform(post("/images")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(getResourceAsString("images/integration/setallimages/json/bodyallimages.json")))
                .andExpect(status().isOk());

        List<String> images = new ArrayList<>();
        Iterable<ImageEntity> imageEntities = imageRepository.findAll();
        imageEntities.forEach(imageEntity -> images.add(imageEntity.getPath()));


        assertThat(images).containsExactlyInAnyOrderElementsOf(imagesToSave);

    }

}
