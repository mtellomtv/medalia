Insert into URL_DOMAIN (ID, NAME) values (1,  'www.domain1.com');
Insert into URL_DOMAIN (ID, NAME) values (2,  'www.domain2.com');
Insert into IMAGES (ID, DOMAIN_ID, PATH) values (1, 1, 'image1.jpg');
Insert into IMAGES (ID, DOMAIN_ID, PATH) values (2, 1, 'somepath/anotherpath/image2.gif');
Insert into IMAGES (ID, DOMAIN_ID, PATH) values (3, 2, 'image3.jpg');
Insert into IMAGES (ID, DOMAIN_ID, PATH) values (4, 2, 'somepath/anotherpath/image4.gif');